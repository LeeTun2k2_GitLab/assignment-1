﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_LeQuangTung
{
    public class NguoiLaoDong
    {
        // Thuộc tính
        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public double LuongCoBan { get; set; }

        // Hàm dựng mặc định
        public NguoiLaoDong()
        {
        }

        // Hàm dựng với tham số
        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            this.HoTen = hoTen;
            this.NamSinh = namSinh;
            this.LuongCoBan = luongCoBan;
        }

        // Phương thức nhập thông tin
        public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
        {
            this.HoTen = hoTen;
            this.NamSinh = namSinh;
            this.LuongCoBan = luongCoBan;
        }

        // Phương thức tính lương
        public double TinhLuong()
        {
            return LuongCoBan;
        }

        // Phương thức xuất thông tin
        public void XuatThongTin()
        {
            Console.WriteLine("Ho ten la: {0}, nam sinh: {1}, luong co ban: {2}.", HoTen, NamSinh, LuongCoBan);
        }
    }
}
