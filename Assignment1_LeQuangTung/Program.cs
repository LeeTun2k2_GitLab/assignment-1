﻿using System;
using System.Collections.Generic;

namespace Assignment1_LeQuangTung
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Nhập số lượng giáo viên từ bàn phím
                Console.Write("Nhập số lượng giáo viên: ");
                int soLuong = int.Parse(Console.ReadLine());

                // Tạo danh sách giáo viên
                List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

                // Nhập thông tin cho từng giáo viên
                for (int i = 0; i < soLuong; i++)
                {
                    Console.WriteLine("\nNhập thông tin giáo viên {0}:", i + 1);
                    GiaoVien giaoVien = new GiaoVien();

                    Console.Write("Họ tên: ");
                    string hoTen = Console.ReadLine();

                    Console.Write("Năm sinh: ");
                    int namSinh = int.Parse(Console.ReadLine());

                    Console.Write("Lương cơ bản: ");
                    double luongCoBan = double.Parse(Console.ReadLine());

                    Console.Write("Hệ số lương: ");
                    double heSoLuong = double.Parse(Console.ReadLine());

                    giaoVien.NhapThongTin(hoTen, namSinh, luongCoBan, heSoLuong);
                    danhSachGiaoVien.Add(giaoVien);
                }

                // Tìm giáo viên có lương thấp nhất
                double luongThapNhat = double.MaxValue;
                GiaoVien giaoVienLuongThapNhat = null;

                foreach (GiaoVien giaoVien in danhSachGiaoVien)
                {
                    double luong = giaoVien.TinhLuong();
                    if (luong < luongThapNhat)
                    {
                        luongThapNhat = luong;
                        giaoVienLuongThapNhat = giaoVien;
                    }
                }

                // Hiển thị thông tin giáo viên có lương thấp nhất
                Console.WriteLine("\nThông tin giáo viên có lương thấp nhất:");
                giaoVienLuongThapNhat.XuatThongTin();
            }
            catch (FormatException)
            {
                Console.WriteLine("Lỗi: Kiểu dữ liệu nhập không đúng.");
            }
            catch (OverflowException)
            {
                Console.WriteLine("Lỗi: Giá trị nhập vào quá lớn hoặc quá nhỏ.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Lỗi: " + ex.Message);
            }

            Console.WriteLine("\nNhấn phím bất kỳ để tiếp tục...");
            Console.ReadKey();
        }
    }
}
