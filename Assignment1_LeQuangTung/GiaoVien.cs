﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_LeQuangTung
{
    public class GiaoVien : NguoiLaoDong
    {
        // Thuộc tính
        public double HeSoLuong { get; set; }

        // Hàm dựng mặc định
        public GiaoVien()
        {

        }

        // Hàm dựng với tham số
        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
            : base(hoTen, namSinh, luongCoBan)
        {
            this.HeSoLuong = heSoLuong;
        }

        // Phương thức nhập thông tin
        public void NhapThongTin(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        {
            base.NhapThongTin(hoTen, namSinh, luongCoBan);
            this.HeSoLuong = heSoLuong;
        }

        // Phương thức tính lương
        public new double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }

        // Phương thức xuất thông tin
        public new void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine("He so luong: {0}, Luong: {1}.", HeSoLuong, TinhLuong());
        }

        // Phương thức xử lý
        public void XuLy()
        {
            HeSoLuong += 0.6;
        }
    }
}
